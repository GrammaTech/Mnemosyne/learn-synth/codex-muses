import importlib.util
import setuptools


spec = importlib.util.spec_from_file_location("pkginfo.version", "src/version.py")
pkginfo = importlib.util.module_from_spec(spec)
spec.loader.exec_module(pkginfo)

with open("README.md") as f:
    long_description = f.read()

with open("requirements.txt") as fd:
    requirements = [r.split("=")[0].rstrip("<>~") for r in fd.readlines()]
    requirements.append(
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master"
    )

with open("requirements-dev.txt") as fd:
    development_requirements = fd.readlines()

setuptools.setup(
    name="codex-muses",
    author="Grammatech",
    description=pkginfo.__description__,
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/codex-muses",
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "src"},
    python_requires=f">={pkginfo.__pythonbaseversion__}",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    entry_points={
        "console_scripts": [
            f"{key} = {value}" for key, value in pkginfo.__scripts__.items()
        ]
    },
    setup_requires=["wheel"],
    install_requires=requirements,
    extras_require={"dev": development_requirements},
)
