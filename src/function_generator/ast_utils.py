"""
Collection of utilities related to ASTs.
"""

from asts import AST, ClassAST, ParseErrorAST, RootAST, FunctionAST, StringAST
from sys import maxsize
from typing import List, Optional, Text, Tuple

Range = Tuple[Tuple[int, int], Tuple[int, int]]


def ast_source_range(
    script: RootAST,
    ast: AST,
) -> Optional[Tuple[Tuple[int, int], Tuple[int, int]]]:
    """
    Return the source range (line, col) for AST in SCRIPT.
    The range is a start, end pair and line and column numbers
    are 1-indexed.
    """
    for c, r in script.ast_source_ranges():
        if c == ast:
            return r


def asts_in_range(script: RootAST, selected: Range) -> List[AST]:
    """
    Return the ASTs in script which overlap the SELECTED LSP range.
    """

    def zero_index(ast_range: Range) -> Range:
        """
        Convert the given AST_RANGE from 1 to 0-indexed.
        """
        ast_beg = (ast_range[0][0] - 1, ast_range[0][1] - 1)  # convert 1 to 0-indexed
        ast_end = (ast_range[1][0] - 1, ast_range[1][1] - 1)  # convert 1 to 0-indexed
        return ast_beg, ast_end

    def adjust_range(ast: AST, ast_range: Range, lines: List[Text]) -> Range:
        """
        Adjust the reported ranges of certain ASTs for our use case.
        """

        def skip_whitespace(point: Tuple[int, int]) -> Tuple[int, int]:
            """
            Adjust the given point to the next non-whitespace position, if possible.
            """
            lineno, column = point
            num_lines = len(lines)

            if lineno + 1 < num_lines:
                for i in range(lineno + 1, num_lines):
                    line = lines[i]
                    if line.strip():
                        break
                lineno = i
                column = len(line) - len(line.lstrip())

            return lineno, column

        def extend_to_eof(point: Tuple[int, int]) -> Tuple[int, int]:
            """
            Adjust point to extend to the end-of-file, if there is only whitespace
            beyond it.
            """
            lineno, column = point
            num_lines = len(lines)

            if lineno + 1 >= num_lines and not lines[lineno][column:].strip():
                column = maxsize

            return lineno, column

        ast_beg, ast_end = ast_range
        if isinstance(ast, FunctionAST):
            # For function ASTs, remove any leading whitespace and add
            # any trailing whitespace, extending to EOF for the end point.
            return skip_whitespace(ast_beg), extend_to_eof(skip_whitespace(ast_end))
        else:
            return ast_beg, ast_end

    def is_selected(ast_range: Range) -> bool:
        """
        Return TRUE if the selected range and AST_RANGE overlap.
        """
        ast_beg, ast_end = ast_range
        sel_beg, sel_end = selected

        # 1. The beginning of the selection falls within the ast or
        # 2. The end of the selection falls within the ast or
        # 3. The beginning of the ast falls within the selection or
        # 4. The end of the ast falls within within the selection
        return (
            (ast_beg <= sel_beg and sel_beg <= ast_end)
            or (ast_beg <= sel_end and sel_end <= ast_end)
            or (sel_beg <= ast_beg and ast_beg <= sel_end)
            or (sel_beg <= ast_end and ast_end <= sel_end)
        )

    lines = script.source_text.split("\n")
    ast_ranges = script.ast_source_ranges()
    ast_ranges = [(ast, zero_index(r)) for ast, r in ast_ranges]
    ast_ranges = [(ast, adjust_range(ast, r, lines)) for ast, r in ast_ranges]
    return [ast for ast, r in ast_ranges if is_selected(r)]


def contains_error_nodes(ast: AST) -> bool:
    """
    Return TRUE if AST contains ParseErrorAST nodes.
    """
    return len([c for c in ast if isinstance(c, ParseErrorAST)]) > 0


def qualified_name(script: RootAST, function: FunctionAST) -> str:
    """
    Return the fully qualified name of FUNCTION in SCRIPT.
    """
    result = ""

    for ast in [function] + list(reversed(function.parents(script))):
        if isinstance(ast, FunctionAST):
            result = (ast.function_name() or "<anon>") + "." + result
        elif isinstance(ast, ClassAST):
            result = ast.name.source_text + "." + result

    return result.rstrip(".")


def find_function(script: RootAST, fname: str) -> Optional[FunctionAST]:
    """
    Return the function in SCRIPT with the given fully qualified function name (FNAME).
    """
    functions = [
        ast
        for ast in script
        if isinstance(ast, FunctionAST) and qualified_name(script, ast) == fname
    ]
    return functions[0] if len(functions) == 1 else None


def function_docstring(function: FunctionAST) -> Optional[StringAST]:
    """
    Return the function AST's documentation string, if possible.
    """
    stmt = function.body.children[0] if function.body.children else None
    docstring = stmt.children[0] if stmt and stmt.children else None

    if isinstance(docstring, StringAST):
        text = docstring.source_text
        if text.startswith('"""') and text.endswith('"""'):
            return docstring
        elif text.startswith("'''") and text.endswith("'''"):
            return docstring


def function_body_without_docstring(function: FunctionAST) -> Optional[AST]:
    """
    Return the body of FUNCTION with the docstring elided, if possible.
    """
    docstring = function_docstring(function)
    children = function.body.child_slot("children")
    children = [child for child in children if docstring not in child]
    return AST.copy(function.body, children=children) if children else None


def function_prototype(function: FunctionAST) -> FunctionAST:
    """
    Return the signature and documentation string of FUNCTION as a function AST.
    """
    children = function.body.child_slot("children")
    docstring_children = [children[0]] if function_docstring(function) else []

    # Create a new function AST with only the docstring in the function body
    # if the given function has an implementation.
    if children != docstring_children:
        body = AST.copy(function.body, children=docstring_children)
        return AST.copy(function, body=body)
    else:
        return function
