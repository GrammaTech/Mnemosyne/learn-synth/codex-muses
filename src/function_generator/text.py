"""
Collection of utilities related to text processing.
"""

from typing import List, Text


def get_indentation(text: Text, lineno: int) -> Text:
    """
    Return the leading whitespace in TEXT at LINENO (1-indexed).
    """
    line = text.splitlines()[lineno - 1]
    leading_ws = len(line) - len(line.lstrip())
    return line[:leading_ws]


def indent(text: Text, indent: Text) -> Text:
    """
    Add INDENT to the front of each line in TEXT.
    """
    lines = text.splitlines()
    lines = [indent + line if line else line for line in lines]
    return "\n".join(lines)


def strip_empty_lines(lines: List[Text]) -> List[Text]:
    """
    Return lines with leading/trailing empty lines elided.
    """
    while lines and not lines[0].strip():
        lines = lines[1:]
    while lines and not lines[-1].strip():
        lines.pop()
    return lines


def has_trailing_newlines(lines: List[Text]) -> bool:
    """
    Return TRUE if LINES ends with two or more newlines.
    """
    return len(lines) > 2 and not lines[-1] and not lines[-2]


def fixup_multiline_strings(lines: List[Text]) -> List[Text]:
    """
    Return LINES with the contents of multiline strings appearing
    at the same level of indentation.  This is a workaround for an
    issue with ASTs.
    """

    def is_multiline_string_delimiter(line: Text) -> bool:
        """
        Return TRUE if line contains a single multiline string delimiter.
        """
        line = line.strip()
        return line == '"""' or line == "'''"

    in_multiline_string = False
    indentation = ""
    result = []

    for line in lines:
        if in_multiline_string:
            line = line.strip()
            line = indentation + line if line else line
        elif is_multiline_string_delimiter(line):
            in_multiline_string = not in_multiline_string
            indentation = line[: len(line) - len(line.lstrip())]

        result.append(line)

    return result
