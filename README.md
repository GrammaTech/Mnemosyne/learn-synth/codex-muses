# Codex Muses

A Python module with a collection of techniques to assist a developer
leveraging OpenAI's Codex model.

## Developer Install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt` and
`pip3 install -r requirement-dev.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

Finally, you will need to clone the
[kitchensink](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink)
repository, install its requirements, and place its src/ directory on
your $PYTHONPATH.  The kitchensink repository is a dependency of this
repository under current development.  This setup may be done with the
following sequence of commands, to be run from the parent directory
of this repository:

```
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/kitchensink.git
cd kitchensink
pip3 install -r requirements.txt
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using this library solely as a client, you may
create a python wheel file from this repository and install
it by executing the following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

Once installed, a `function-generator-server` command will be added
to your $PATH which may be invoked directly using the same interface
and options described in the function generator section below.

# Command Line Interface

## Function Generator

For developers, the `server.py` script in `src/function_generator`
defines a command line interface for this tool.  Help documentation
is available by running `server.py --help`.

The script spawns an LSP server and listens for code action requests,
returning code actions inserting function bodies when a function prototype
and documention string have been provided.  The function bodies are
generated using OpenAI's Codex machine learning model.
