__packagename__ = "codex-muses"
__version__ = "0.0.0"
__pythonbaseversion__ = "3.6.0"
__description__ = "A Python module with a collection of techniques to assist a developer leveraging OpenAI's Codex model."
__scripts__ = {"function-generator-server": "function_generator.server:main"}
